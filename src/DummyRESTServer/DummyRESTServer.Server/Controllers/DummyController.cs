﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DummyRESTServer.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DummyController : ControllerBase
    {
        private readonly ILogger<DummyController> _logger;

        public DummyController(ILogger<DummyController> logger)
        {
            _logger = logger;
        }

        [HttpGet("OK")]
        public IActionResult GetOK() => ReturnOkResponse();

        [HttpGet("BadREquest")]
        public IActionResult GetBadRequest() => ReturnBadRequestResponse();       

        [HttpGet("Random")]
        public IActionResult GetRandom()
        {
            var radom = new Random();

            var rand = radom.Next(1, 3);

            if(rand == 1)
            {
                return ReturnBadRequestResponse();
            }

            return ReturnOkResponse();
        }

        private IActionResult ReturnOkResponse()
        {
            _logger.LogInformation($"{DateTime.Now.ToShortTimeString()} {Request.Host.Host}. Response: OK");
            return Ok();
        }

        private IActionResult ReturnBadRequestResponse()
        {
            _logger.LogInformation($"{DateTime.Now.ToShortTimeString()} {Request.Host.Host}. Response: BadRequest");
            return BadRequest();
        }
    }
}
